#ifndef SERIAL_H
#define SERIAL_H

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QStringList>
#include <QMessageBox>
class serial : public QObject
{
    Q_OBJECT
public:
    explicit serial(QObject *parent = nullptr);
    QStringList scanSerial();                           //扫描串口
    bool open(QString serialName, int baudRate);        //打开串口
    void close();                                       //关闭串口
    void sendData(QByteArray &sendData);                //发送数据
    QByteArray getReadBuf();                            //读取缓存区数据
    void clearReadBuf();                                //清零缓存区
    QByteArray hexStringToByteArray(QString HexString);

private:
    QSerialPort *m_serialPort;
    QByteArray m_readBuf; // 存储下位机发来数据的缓冲区

signals:
    void readSignal(); // 当下位机中有数据发送过来时就会触发这个信号，以提示其它类对象

public slots:

    void readData();                                    //读取数据
};

#endif // SERIAL_H
