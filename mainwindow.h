#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QDebug>

#include <QByteArray>
#include <QTextCodec>
#include "serial.h"


#include <QSerialPort>
#include <QSerialPortInfo>
#include <QStringList>

#include <QTimer>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    serial *m_serial;
    QTimer *m_timer;

private:
    Ui::MainWindow *ui;
    bool openflag;
    QStringList Port_List;
    void initTimer();

private slots:
    void pBtn_onClicked();
    void pBtn2_onClicked();
    void pBtn3_onClicked();
    void pBtn4_onClicked();
    void readSerialData();  // 读取从自定义串口类获得的数据
    void m_timerEvent();      //定时器触发函数
};

#endif // MAINWINDOW_H
